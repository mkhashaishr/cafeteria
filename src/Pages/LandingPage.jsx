import React, {useEffect} from "react";
import Header from "../Components/LandingPage/Header";
import Hero from "../Components/LandingPage/Hero";
import About from "../Components/LandingPage/About";
import WhatWeDo from "../Components/LandingPage/WhatWeDo";
import CaseStudies from "../Components/LandingPage/CaseStudies";
import Conctact from "../Components/LandingPage/Contact";
import Footer from "../Components/LandingPage/Footer"
import { Stack } from "react-bootstrap";

const LandingPage = () => {

    useEffect(() => {
        document.title = "Cafetería - Solution for your Coffee Shop"
    }, [])

    return (
        <div className="landing-page">
            <Stack>
                <Header />
                <Hero />
                <About />
                <WhatWeDo />
                <CaseStudies />
                <Conctact />
                <Footer />
            </Stack>
        </div>
    )
}

export default LandingPage;