import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import imgFooter from "../../Assets/Img/cafeteria.svg"

const Footer = () => {
    return (
        <div className="footer">
            <div className="svg">
                <svg id="wave" style={{transform:"rotate(0deg)",transition: "0.3s"}} viewBox="0 0 1440 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                    <path style={{transform:"translate(0, 0px)", opacity:"1"}} fill="#161616" d="M0,0L60,8.3C120,17,240,33,360,33.3C480,33,600,17,720,18.3C840,20,960,40,1080,48.3C1200,57,1320,53,1440,43.3C1560,33,1680,17,1800,21.7C1920,27,2040,53,2160,68.3C2280,83,2400,87,2520,85C2640,83,2760,77,2880,71.7C3000,67,3120,63,3240,58.3C3360,53,3480,47,3600,40C3720,33,3840,27,3960,25C4080,23,4200,27,4320,36.7C4440,47,4560,63,4680,60C4800,57,4920,33,5040,28.3C5160,23,5280,37,5400,40C5520,43,5640,37,5760,40C5880,43,6000,57,6120,63.3C6240,70,6360,70,6480,65C6600,60,6720,50,6840,53.3C6960,57,7080,73,7200,75C7320,77,7440,63,7560,56.7C7680,50,7800,50,7920,53.3C8040,57,8160,63,8280,55C8400,47,8520,23,8580,11.7L8640,0L8640,100L8580,100C8520,100,8400,100,8280,100C8160,100,8040,100,7920,100C7800,100,7680,100,7560,100C7440,100,7320,100,7200,100C7080,100,6960,100,6840,100C6720,100,6600,100,6480,100C6360,100,6240,100,6120,100C6000,100,5880,100,5760,100C5640,100,5520,100,5400,100C5280,100,5160,100,5040,100C4920,100,4800,100,4680,100C4560,100,4440,100,4320,100C4200,100,4080,100,3960,100C3840,100,3720,100,3600,100C3480,100,3360,100,3240,100C3120,100,3000,100,2880,100C2760,100,2640,100,2520,100C2400,100,2280,100,2160,100C2040,100,1920,100,1800,100C1680,100,1560,100,1440,100C1320,100,1200,100,1080,100C960,100,840,100,720,100C600,100,480,100,360,100C240,100,120,100,60,100L0,100Z">
                    </path>
                </svg>
                <svg id="wave2" style={{transform:"rotate(0deg)",transition: "0.3s"}} viewBox="0 0 1440 100" version="1.1" xmlns="http://www.w3.org/2000/svg">
                    <path style={{transform:"translate(0, 0px)", opacity:"1"}} fill="#1A202C" d="M0,0L60,8.3C120,17,240,33,360,33.3C480,33,600,17,720,18.3C840,20,960,40,1080,48.3C1200,57,1320,53,1440,43.3C1560,33,1680,17,1800,21.7C1920,27,2040,53,2160,68.3C2280,83,2400,87,2520,85C2640,83,2760,77,2880,71.7C3000,67,3120,63,3240,58.3C3360,53,3480,47,3600,40C3720,33,3840,27,3960,25C4080,23,4200,27,4320,36.7C4440,47,4560,63,4680,60C4800,57,4920,33,5040,28.3C5160,23,5280,37,5400,40C5520,43,5640,37,5760,40C5880,43,6000,57,6120,63.3C6240,70,6360,70,6480,65C6600,60,6720,50,6840,53.3C6960,57,7080,73,7200,75C7320,77,7440,63,7560,56.7C7680,50,7800,50,7920,53.3C8040,57,8160,63,8280,55C8400,47,8520,23,8580,11.7L8640,0L8640,100L8580,100C8520,100,8400,100,8280,100C8160,100,8040,100,7920,100C7800,100,7680,100,7560,100C7440,100,7320,100,7200,100C7080,100,6960,100,6840,100C6720,100,6600,100,6480,100C6360,100,6240,100,6120,100C6000,100,5880,100,5760,100C5640,100,5520,100,5400,100C5280,100,5160,100,5040,100C4920,100,4800,100,4680,100C4560,100,4440,100,4320,100C4200,100,4080,100,3960,100C3840,100,3720,100,3600,100C3480,100,3360,100,3240,100C3120,100,3000,100,2880,100C2760,100,2640,100,2520,100C2400,100,2280,100,2160,100C2040,100,1920,100,1800,100C1680,100,1560,100,1440,100C1320,100,1200,100,1080,100C960,100,840,100,720,100C600,100,480,100,360,100C240,100,120,100,60,100L0,100Z">
                    </path>
                </svg>
            </div>
            <div className="content">
                <Container>
                    <Row className="p-0 g-0">
                        <Col md="6" xl="3">
                            <img src={imgFooter} alt="Footer" className="mb-2" />
                            <p className="text">Curabitur tempor ac metus vel commodo. Pellentesque elementum ligula id ipsum fermentum volutpat.</p>
                        </Col>
                        <Col md="6" xl="2" className="offset-xl-2 mb-3 mb-md-0">
                            <h4 className="mb-1 mb-lg-3">Navigation</h4>
                            <ul className="text">
                                <li>
                                    <NavLink to="/">Home</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/">About</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/">Service</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/">Show Case</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/">Contact</NavLink>
                                </li>
                            </ul>
                        </Col>
                        <Col md="6" xl="2" className="mb-3 mb-md-0 mt-0 mt-md-4 mt-xl-0">
                            <h4 className="mb-1 mb-lg-3">Service</h4>
                            <ul className="text">
                                <li>
                                    <NavLink to="/">Construction</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/">Decoration</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/">Equipment</NavLink>
                                </li>
                            </ul>
                        </Col>
                        <Col md="6" xl="2" className="mb-3 mb-md-0 mt-0 mt-md-4 mt-xl-0">
                            <h4 className="mb-1 mb-lg-3">Information</h4>
                            <ul className="text">
                                <li>
                                    <NavLink to="/">Terms & Conditions</NavLink>
                                </li>
                                <li>
                                    <NavLink to="/">Privacy Policy</NavLink>
                                </li>
                            </ul>
                        </Col>
                    </Row>
                    <Row>
                        <p className="footer-footer text">Copyright © 2021 <span>Cafetería</span>. All Rights Reserved</p>
                    </Row>
                </Container>
            </div>
        </div>
    )
}

export default Footer;