import React from "react";
import { Col, Container, Row, Stack, Button } from "react-bootstrap";
import imgConstruct from '../../Assets/Img/construct.png';
import imgDecoration from '../../Assets/Img/decor.png';
import imgEquipment from '../../Assets/Img/rquipment.png';

const WhatWeDo = () => {
    return (
        <div data-bs-spy="scroll" data-bs-target="#navbar-example2" data-bs-offset="0" className="scrollspy-example" tabindex="0">
            <div className="whatwedo-section" id="service">
                <Container>
                    <Row className="text-center justify-content-center p-0 g-0 whatwedo-section--header">
                        <Col sm="12">                        
                            <span className="header-section mt-4 mt-lg-0">What we do</span>                                       
                        </Col>
                        <Col md="9" lg="6">
                            <span className="text">Proin vitae nibh a mi fringilla iaculis in vel tellus. Maecenas mi mauris, ultrices eu porttitor non, suscipit at elit. Quisque viverra diam a blandit dignissim.</span>
                        </Col>
                    </Row>
                    <Row className="justify-content-center d-flex align-items-center p-0 g-0">
                        <Col md="5" className="p-0 g-0 d-flex justify-content-center mb-4 mb-xl-0">
                            <img src={imgConstruct} alt="Construction" className="img-fluid" />
                        </Col>
                        <Col md="6" xl="4" className="offset-md-1 g-0 p-0">
                            <Stack>
                                <span className="desc-header">Coffee Shop Construction</span>
                                <span className="text my-2 my-lg-3">Cras vitae tortor enim. Curabitur vestibulum lacinia malesuada. Aliquam neque justo, lacinia quis felis id, ullamcorper pulvinar leo.</span>
                                <div>
                                    <Button variant="primary" className="py-1 px-3">Details</Button>
                                </div>
                            </Stack>
                        </Col>
                    </Row>
                    <Row className="justify-content-start justify-content-xl-center d-flex align-items-center p-0 g-0 my-5 my-lg-4 d-flex">
                        <Col md="6" xl="4" className="g-0 p-0 order-2 order-md-1">
                            <Stack>
                                <span className="desc-header">Coffee Shop Decoration</span>
                                <span className="text my-2 my-lg-3">Cras vitae tortor enim. Curabitur vestibulum lacinia malesuada. Aliquam neque justo, lacinia quis felis id, ullamcorper pulvinar leo.</span>
                                <div>
                                    <Button variant="primary" className="py-1 px-3">Details</Button>
                                </div>
                            </Stack>
                        </Col>
                        <Col md="5" className="offset-md-1 p-0 g-0 order-1 order-lg-2 d-flex justify-content-center mb-4 mb-xl-0">
                            <img src={imgDecoration} alt="Construction" className="img-fluid" />
                        </Col>
                    </Row>
                    <Row className="justify-content-center d-flex align-items-center p-0 g-0">
                        <Col md="5" className="p-0 g-0 d-flex justify-content-center mb-4 mb-xl-0">
                            <img src={imgEquipment} alt="Construction" className="img-fluid" />
                        </Col>
                        <Col md="6" xl="4" className="offset-md-1 g-0 p-0">
                            <Stack>
                                <span className="desc-header">Coffee Shop Equipment</span>
                                <span className="text my-2 my-lg-3">Cras vitae tortor enim. Curabitur vestibulum lacinia malesuada. Aliquam neque justo, lacinia quis felis id, ullamcorper pulvinar leo.</span>
                                <div>
                                    <Button variant="primary" className="py-1 px-3">Details</Button>
                                </div>
                            </Stack>
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    )
}

export default WhatWeDo;