import React from "react";
import { Container, Row, Col, Stack, Card} from "react-bootstrap";
import { NavLink } from "react-router-dom";
import imgCs1 from "../../Assets/Img/cs1.png";
import imgCs2 from "../../Assets/Img/cs2.png";
import imgCs3 from "../../Assets/Img/cs3.png";
import imgCs4 from "../../Assets/Img/cs4.png";
import imgCs5 from "../../Assets/Img/cs5.png";
import imgCs6 from "../../Assets/Img/cs6.png";

const CaseStudies = () => {
    return (
        <div data-bs-spy="scroll" data-bs-target="#navbar-example2" data-bs-offset="0" className="scrollspy-example" tabindex="0">
            <div className="casestudies-section" id="case-studies">
                <Container>
                    <Row className="text-center justify-content-center p-0 g-0 casestudies-section--header">
                        <Col sm="12">                        
                            <span className="header-section mt-4 mt-lg-0">Case Studies</span>                                       
                        </Col>
                        <Col md="9" lg="6"  >
                            <span className="text">Curabitur egestas porta magna sed aliquam. Curabitur at turpis vitae risus cursus commodo. Donec sed magna augue. </span>
                        </Col>
                    </Row>
                    <Row className="justify-content-center p-0 g-0">
                        <Col xl="10">
                            <Row className="g-0 p-0">
                                <Col md="6" xl="4" className="mb-4">
                                    <Stack className="m-2">
                                        <Card className="border-0 c">
                                            <Card.Body className="p-0 g-0 c--body">
                                                <p className="text bg-primary py-2 px-3 category">Decoration</p>
                                                <h3 className="text mb-3">COPPER CLUB Specialty Coffee</h3>
                                                <img src={imgCs1} alt="Case Studies 1" className="w-100 mb-3"/>
                                                <p className="text">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed sed finibus lectus. </p>
                                                <NavLink to="/" className="text text-gray-900">Read More</NavLink>
                                            </Card.Body>
                                        </Card>
                                    </Stack>
                                </Col>
                                <Col md="6" xl="4" className="mb-4">
                                    <Stack className="m-2">
                                        <Card className="border-0 c">
                                            <Card.Body className="p-0 g-0 c--body">
                                                <p className="text bg-primary py-2 px-3 category">Decoration</p>
                                                <h3 className="text mb-3">Contrive Coffee & Space</h3>
                                                <img src={imgCs2} alt="Case Studies 1" className="w-100 mb-3" />
                                                <p className="text">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed sed finibus lectus. </p>
                                                <NavLink to="/" className="text text-gray-900">Read More</NavLink>
                                            </Card.Body>
                                        </Card>
                                    </Stack>
                                </Col>
                                <Col md="6" xl="4" className="mb-4">
                                    <Stack className="m-2">
                                        <Card className="border-0 c">
                                            <Card.Body className="p-0 g-0 c--body">
                                                <p className="text bg-primary py-2 px-3 category">Decoration</p>
                                                <h3 className="text mb-3">Stuja Coffee Indonesia</h3>
                                                <img src={imgCs3} alt="Case Studies 1" className="w-100 mb-3" />
                                                <p className="text">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed sed finibus lectus. </p>
                                                <NavLink to="/" className="text text-gray-900">Read More</NavLink>
                                            </Card.Body>
                                        </Card>
                                    </Stack>
                                </Col>
                                <Col md="6" xl="4" className="mb-4">
                                    <Stack className="m-2">
                                        <Card className="border-0 c">
                                            <Card.Body className="p-0 g-0 c--body">
                                                <p className="text bg-primary py-2 px-3 category">Equipment</p>
                                                <h3 className="text mb-3">Abelton Coffee</h3>
                                                <img src={imgCs4} alt="Case Studies 1" className="w-100 mb-3" />
                                                <p className="text">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed sed finibus lectus. </p>
                                                <NavLink to="/" className="text text-gray-900">Read More</NavLink>
                                            </Card.Body>
                                        </Card>
                                    </Stack>
                                </Col>
                                <Col md="6" xl="4" className="mb-4">
                                    <Stack className="m-2">
                                        <Card className="border-0 c">
                                            <Card.Body className="p-0 g-0 c--body">
                                                <p className="text bg-primary py-2 px-3 category">Decoration</p>
                                                <h3 className="text mb-3">Cotta Coffee & Taman</h3>
                                                <img src={imgCs5} alt="Case Studies 1" className="w-100 mb-3" />
                                                <p className="text">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed sed finibus lectus. </p>
                                                <NavLink to="/" className="text text-gray-900">Read More</NavLink>
                                            </Card.Body>
                                        </Card>
                                    </Stack>
                                </Col>
                                <Col md="6" xl="4" className="mb-4">
                                    <Stack className="m-2">
                                        <Card className="border-0 c">
                                            <Card.Body className="p-0 g-0 c--body">
                                                <p className="text bg-primary py-2 px-3 category">Equipment</p>
                                                <h3 className="text mb-3">Rusters Coffee</h3>
                                                <img src={imgCs6} alt="Case Studies 1" className="w-100 mb-3" />
                                                <p className="text">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed sed finibus lectus. </p>
                                                <NavLink to="/" className="text text-gray-900">Read More</NavLink>
                                            </Card.Body>
                                        </Card>
                                    </Stack>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    )
}

export default CaseStudies;