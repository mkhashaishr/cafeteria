import React, {useState} from "react";
import { Stack, Row, Col, Button } from "react-bootstrap";
import imgLogo from "../../Assets/Img/cafeteria.svg";
import imgHero from "../../Assets/Img/herologo.svg";

const Routes = () => {
    const [navbar, setNavbar] = useState(false);

    const changeBackround = () => {
        if (window.scrollY >= 80) {
            setNavbar(true)
        } else {
            setNavbar(false)
        }
    }

    window.addEventListener('scroll', changeBackround);

    return (
        <div data-bs-spy="scroll" data-bs-target="#navbar-example2" data-bs-offset="0" className="scrollspy-example" tabindex="0">
            <div className="header" id="header">
                <nav className={navbar ?  `navbar active fixed-top navbar-expand-lg navbar-dark p-md-3` : `navbar fixed-top navbar-expand-lg navbar-dark p-md-3`}>
                    <div className="container">
                        <a className="navbar-brand" href="/#header">
                            <img src={imgLogo} alt="Web Logo" />
                        </a>
                        <button
                        className="navbar-toggler"
                        type="button"
                        data-bs-toggle="collapse"
                        data-bs-target="#navbarNav"
                        aria-controls="navbarNav"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                        >
                        <span className="navbar-toggler-icon"></span>
                        </button>

                        <div className="collapse navbar-collapse" id="navbarNav">
                        <div className="mx-auto"></div>
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <a href="/#header" className="nav-link text-white">Home</a>
                            </li>
                            <li className="nav-item">
                                <a href="#about" className="nav-link text-white">About</a>
                            </li>
                            <li className="nav-item">
                                <a href="/#service" className="nav-link text-white">Service</a>
                            </li>
                            <li className="nav-item">
                                <a href="/#case-studies" className="nav-link text-white">Our Works</a>
                            </li>
                            <li className="nav-item">
                                <a href="/#contact"  className="nav-link text-white">Contact</a>
                            </li>
                        </ul>
                        </div>
                    </div>
                </nav>
                <div className="banner-image w-100 d-flex justify-content-center align-items-center">
                <div className="content text-center">
                    <Stack gap={4}>                
                        <div className="d-flex justify-content-center">
                            <img src={imgHero} alt="Hero Logo" style={{width: "140px", height: "auto"}}/>
                        </div>
                        <h1 className="text-white">Solution for your Coffee Shop</h1>
                        <Row className="justify-content-center p-2 p-lg-0">
                            <Col xl="7">
                                <p>Unc sollicitudin pellentesque risus in iaculis. Aenean velit neque, mattis et ultricies sit amet, tempor sollicitudin risus. Cras sit amet vehicula mi, id ultricies orci. </p>
                            </Col>
                        </Row>
                        <div>
                            <Button variant="primary">Join Us</Button>
                        </div>
                    </Stack>
                </div>
                </div>
            </div>
        </div>
    )
}

export default Routes;