import React from "react";
import { Col, Container, Stack, Row} from "react-bootstrap";
import imgAbout from "../../Assets/Img/about.png"

const About = () => {
    return (
        <div data-bs-spy="scroll" data-bs-target="#navbar-example2" data-bs-offset="0" className="scrollspy-example" tabindex="0">
            <div className="about-section" id="about">
                <Container>
                    <Row className="justify-content-start justify-content-lg-center align-items-center p-0 g-0">
                        <Col md="5" lg="4" className="p-0 g-0">
                            <img src={imgAbout} alt="About" className="img-fluid p-0 p-lg-0"/>
                        </Col>
                        <Col md="6" lg="5" className="offset-md-1 g-0 p-0">
                            <Stack>
                                <span className="header-section mt-4 mt-lg-0">About Us</span>
                                <span className="text">Nam suscipit massa odio, nec blandit mi condimentum vel. Suspendisse lacinia, neque at mollis consectetur, ante risus finibus ipsum, vel mollis augue orci vitae magna. Pellentesque mauris velit, tincidunt sit amet nisl non, sagittis commodo lacus.</span>
                            </Stack>
                        </Col>
                    </Row>    
                </Container>            
            </div>
        </div>
    )
}

export default About;